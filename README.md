# Swiss Canton Enum

Rust enum crate for Swiss-canton-specific code

# Example

You can use the enum to control something in your code:

```rust
use swiss_canton::SwissCanton;

let canton = SwissCanton::ZH;

match canton {
    SwissCanton::ZH => println!("Salü!"),
    SwissCanton::NE => println!("Bonjour !"),
    _ => println!("Grüezi!")
}
```

You can also parse, compare and print the short code of the canton:

```rust
use swiss_canton::SwissCanton;

let parsed_canton = "TI".parse::<SwissCanton>().unwrap();

if (parsed_canton.eq(&SwissCanton::TI)) {
    println!("Buongiorno in {}!", SwissCanton::TI);
}
```
