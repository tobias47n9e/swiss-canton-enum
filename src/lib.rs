//! Swiss canton enum
//!
//! A crate that can help your code do canton-specific things
//!
//! # Example
//!
//! You can use the enum to control something in your code:
//!
//! ```
//! use swiss_canton::SwissCanton;
//!
//! let canton = SwissCanton::ZH;
//!
//! match canton {
//!     SwissCanton::ZH => println!("Salü!"),
//!     SwissCanton::NE => println!("Bonjour !"),
//!     _ => println!("Grüezi!")
//! }
//! ```
//!
//! You can also parse, compare and print the short code of the canton:
//!
//! ```
//! use swiss_canton::SwissCanton;
//!
//! let parsed_canton = "TI".parse::<SwissCanton>().unwrap();
//!
//! if (parsed_canton.eq(&SwissCanton::TI)) {
//!     println!("Buongiorno in {}!", SwissCanton::TI);
//! }
//! ```

#[macro_use]
extern crate failure;

use std::fmt::Formatter;

/// Swiss Canton Enum
///
/// If your code has to do canton-specific things you can use this enum.
///
/// # Example
///
/// You can use the enum to control something in your code:
///
/// ```
/// use swiss_canton::SwissCanton;
///
/// let canton = SwissCanton::ZH;
///
/// match canton {
///     SwissCanton::ZH => println!("Salü!"),
///     SwissCanton::NE => println!("Bonjour !"),
///     _ => println!("Grüezi!")
/// }
/// ```
///
/// You can also parse, compare and print the short code of the canton:
///
/// ```
/// use swiss_canton::SwissCanton;
///
/// let parsed_canton = "TI".parse::<SwissCanton>().unwrap();
///
/// if (parsed_canton.eq(&SwissCanton::TI)) {
///     println!("Buongiorno in {}!", SwissCanton::TI);
/// }
/// ```
#[derive(Debug, PartialEq)]
pub enum SwissCanton {
    /// Aargau
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::AG;
    /// ```
    AG,

    /// Appenzell Innerrhoden
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::AI;
    /// ```
    AI,

    /// Appenzell Ausserrhoden
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::AR;
    /// ```
    AR,

    /// Bern
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::BE;
    /// ```
    BE,

    /// Basel-Landschaft
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::BL;
    /// ```
    BL,

    /// Basel-Stadt
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::BS;
    /// ```
    BS,

    /// Fribourg
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::FR;
    /// ```
    FR,

    /// Geneva
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::GE;
    /// ```
    GE,

    /// Glarus
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::GL;
    /// ```
    GL,

    /// Graubünden, Grisons
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::GR;
    /// ```
    GR,

    /// Jura
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::JU;
    /// ```
    JU,

    /// Luzern
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::LU;
    /// ```
    LU,

    /// Neuchâtel
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::NE;
    /// ```
    NE,

    /// Nidwalden
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::NW;
    /// ```
    NW,

    /// Obwalden
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::OW;
    /// ```
    OW,

    /// St. Gallen
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::SG;
    /// ```
    SG,

    /// Schaffhausen
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::SH;
    /// ```
    SH,

    /// Solothurn
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::SO;
    /// ```
    SO,

    /// Schwyz
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::SZ;
    /// ```
    SZ,

    /// Thurgau
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::TG;
    /// ```
    TG,

    /// Ticino
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::TI;
    /// ```
    TI,

    /// Uri
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::UR;
    /// ```
    UR,

    /// Vaud
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::VD;
    /// ```
    VD,

    /// Valais
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::VS;
    /// ```
    VS,

    /// Zug
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::ZG;
    /// ```
    ZG,

    /// Zürich
    ///
    /// # Example
    ///
    /// ```
    /// use swiss_canton::SwissCanton;
    /// SwissCanton::ZH;
    /// ```
    ZH,
}

#[derive(Debug, Fail)]
pub enum SwissCantonError {
    #[fail(display = "Invalid Swiss canton short code supplied: {}", name)]
    InvalidCantonShortCode { name: String },
}

impl std::str::FromStr for SwissCanton {
    type Err = SwissCantonError;

    fn from_str(canton_short_code: &str) -> Result<Self, Self::Err> {
        match canton_short_code {
            "AG" => Ok(SwissCanton::AG),
            "AI" => Ok(SwissCanton::AI),
            "AR" => Ok(SwissCanton::AR),
            "BE" => Ok(SwissCanton::BE),
            "BL" => Ok(SwissCanton::BL),
            "BS" => Ok(SwissCanton::BS),
            "FR" => Ok(SwissCanton::FR),
            "GE" => Ok(SwissCanton::GE),
            "GL" => Ok(SwissCanton::GL),
            "GR" => Ok(SwissCanton::GR),
            "JU" => Ok(SwissCanton::JU),
            "LU" => Ok(SwissCanton::LU),
            "NE" => Ok(SwissCanton::NE),
            "NW" => Ok(SwissCanton::NW),
            "OW" => Ok(SwissCanton::OW),
            "SG" => Ok(SwissCanton::SG),
            "SH" => Ok(SwissCanton::SH),
            "SO" => Ok(SwissCanton::SO),
            "SZ" => Ok(SwissCanton::SZ),
            "TG" => Ok(SwissCanton::TG),
            "TI" => Ok(SwissCanton::TI),
            "UR" => Ok(SwissCanton::UR),
            "VD" => Ok(SwissCanton::VD),
            "VS" => Ok(SwissCanton::VS),
            "ZG" => Ok(SwissCanton::ZG),
            "ZH" => Ok(SwissCanton::ZH),
            _ => Err(SwissCantonError::InvalidCantonShortCode {
                name: canton_short_code.to_string(),
            }),
        }
    }
}

impl std::fmt::Display for SwissCanton {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            SwissCanton::AG => f.write_str("AG"),
            SwissCanton::AI => f.write_str("AI"),
            SwissCanton::AR => f.write_str("AR"),
            SwissCanton::BE => f.write_str("BE"),
            SwissCanton::BL => f.write_str("BL"),
            SwissCanton::BS => f.write_str("BS"),
            SwissCanton::FR => f.write_str("FR"),
            SwissCanton::GE => f.write_str("GE"),
            SwissCanton::GL => f.write_str("GL"),
            SwissCanton::GR => f.write_str("GR"),
            SwissCanton::JU => f.write_str("JU"),
            SwissCanton::LU => f.write_str("LU"),
            SwissCanton::NE => f.write_str("NE"),
            SwissCanton::NW => f.write_str("NW"),
            SwissCanton::OW => f.write_str("OW"),
            SwissCanton::SG => f.write_str("SG"),
            SwissCanton::SH => f.write_str("SH"),
            SwissCanton::SO => f.write_str("SO"),
            SwissCanton::SZ => f.write_str("SZ"),
            SwissCanton::TG => f.write_str("TG"),
            SwissCanton::TI => f.write_str("TI"),
            SwissCanton::UR => f.write_str("UR"),
            SwissCanton::VD => f.write_str("VD"),
            SwissCanton::VS => f.write_str("VS"),
            SwissCanton::ZG => f.write_str("ZG"),
            SwissCanton::ZH => f.write_str("ZH"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::SwissCanton;

    #[test]
    fn parse_from_string() {
        let canton = "TG".parse::<SwissCanton>().unwrap();
        assert_eq!(canton, SwissCanton::TG);
    }

    #[test]
    #[should_panic]
    fn unknown_canton() {
        // Switzerland is not a canton
        "CH".parse::<SwissCanton>().unwrap();
    }

    #[test]
    fn print_string() {
        assert_eq!(SwissCanton::ZH.to_string(), "ZH");
    }

    #[test]
    fn compare() {
        assert!(SwissCanton::AR.ne(&SwissCanton::AI));
    }
}
